
interface Geometric_shape {
    void draw_shape();
}


class Rectangle implements Geometric_shape {
    @Override
    public void draw_shape() {
        System.out.println("Rectangle class::draw_shape() method.");
    }
}
class Square implements Geometric_shape {
    @Override
    public void draw_shape() {
        System.out.println("Square class::draw_shape() method.");
    }
}

class Circle implements Geometric_shape {
    @Override
    public void draw_shape() {
        System.out.println("Circle class::draw_shape() method.");
    }
}

class ShapeFactory {
  
    public Geometric_shape shapeObject(String shapeType){
        if(shapeType == null){
            return null;
        }
      
        if(shapeType.equalsIgnoreCase("Circle")){
            return new Circle();

             
        } else if(shapeType.equalsIgnoreCase("Rectangle")){
            return new Rectangle();

   
        } else if(shapeType.equalsIgnoreCase("Square")){
            return new Square();
        }
        return null;
    }
}
public class Main {

    public static void main(String[] args) {
  
        ShapeFactory shapeFactory = new ShapeFactory();


        Geometric_shape shape_Circle = shapeFactory.shapeObject("CIRCLE");

  
        shape_Circle.draw_shape();

  
        Geometric_shape shape_Rectangle = shapeFactory.shapeObject("RECTANGLE");

   
        shape_Rectangle.draw_shape();


        Geometric_shape shape_Square = shapeFactory.shapeObject("SQUARE");

 
        shape_Square.draw_shape();
    }
}